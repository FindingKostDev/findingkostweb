<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->model('akun_model');
        $this->load->model('kos_model');
	}
    
	public function index()
    {
        // TES KOS_MODEL 1
        $test_name       = 'Test method kos() dengan parameter 2';
        $test            = $this->kos_model->kos(2);
        $expected_result = $this->test_kos_param_2();
        $notes           = 'Class kos_model <br>Hasil yang diinginkan adalah data pada tabel kos join akun dengan id 2.';
        $this->unit->run( $test, $expected_result, $test_name, $notes );
        
        // TES KOS_MODEL 2
        $test_name       = 'Test method kos() dengan parameter -1';
        $test            = $this->kos_model->kos(-1);
        $expected_result = NULL;
        $notes           = 'Class kos_model <br>Hasil yang diinginkan tidak ada di dalam database.';
        $this->unit->run( $test, $expected_result, $test_name, $notes );
        
        // TES KOS_MODEL 3
        $test_name       = 'Test method hitung() dengan return 7';
        $test            = $this->kos_model->hitung();
        $expected_result = 7;
        $notes           = 'Class kos_model <br>Hasil yang diinginkan 7 baris yang ada di tabel kos.';
        $this->unit->run( $test, $expected_result, $test_name, $notes );
        
        // TES AKUN_MODEL 1
        $test_name       = 'Test method cek_akun() dengan parameter administrator';
        $test            = $this->akun_model->cek_akun('adMin');
        $expected_result = $this->test_akun_administrator ();
        $notes           = 'Class akun_model <br>Parameter menggunakan camelCase untuk mengetahui sensitif case dari input <br>Hasil yang diinginkan adalah data pada tabel akun dengan username admin.';
        $this->unit->run( $test, $expected_result, $test_name, $notes );
        echo '<pre>';
        var_dump($this->akun_model->cek_akun('adMin'));
        echo '</pre>';
        
        echo $this->unit->report();
	}
    
    private function test_kos_param_2() 
    {
        $obj            = new stdClass();
        $obj->id        = "5"; 
        $obj->alamat    = "Mulyosari Utara VII Mulyosari Surabaya";
        $obj->daerah    = "Mulyosari";
        $obj->id_pemilik= "5";
        $obj->foto      = "foto.jpg";
        $obj->fasilitas = "Kamar tidur <br>Kamar mandi <br>Parkir";
        $obj->harga     = "500000";
        $obj->username  = "dewangga";
        $obj->password  = "4d1a65f1c6d24c1f8f714fe7e31d29fc";
        $obj->nama      = "Dewangga Prasetya Praja";
        $obj->tipe      = "1";
        $obj->email     = "dewangga@gmail.com";
        $obj->telepon   = "99389";
        
        return $obj;
    }
    
    private function test_akun_administrator() 
    {
        $obj            = new stdClass();
        $obj->id        = "999";
        $obj->username  = "admin";
        $obj->password  = "202cb962ac59075b964b07152d234b70";
        $obj->nama      = "Administrator";
        $obj->tipe      = "0";
        $obj->email     = "findingkost@gmail.com";
        $obj->telepon   = "0";
        $obj->alamat    ="ITS";
        return $obj;
    }
    
}