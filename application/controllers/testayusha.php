<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class testayusha extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->model('akun_model');
        $this->load->model('Kos_model');
	}

	public function index() {
		$testname 		= 'Tes alamat akun';
		$result			= $this->Kos_model->alamat_pemilik(1);
		$expected		= 'Denpasar';

		$this->unit->run($result,$expected,$testname);
		$this->load->view('hasiltes');

	}
}
?>