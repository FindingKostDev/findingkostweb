<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class testgalih extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->model('akun_model');
        $this->load->model('Kos_model');
	}

	public function index() {
		$testname 		= 'Tes nama akun';
		$result			= $this->Kos_model->nama_pemilik(2);
		$expected		= 'Redian Galih Irianti';

		$this->unit->run($result,$expected,$testname);

		$test_name       = 'Test method cek_akun() dengan parameter rediangalih';
        $test            = $this->akun_model->cek_akun('rediangalih');
        $expected_result = $this->test_akun_pemilik ();
        $this->unit->run( $test, $expected_result, $test_name );

		$this->load->view('hasiltes');
	}

	private function test_akun_pemilik() 
    {
        $obj            = new stdClass();
        $obj->id        = "2";
        $obj->username  = "rediangalih";
        $obj->password  = "674f3c2c1a8a6f90461e8a66fb5550ba";
        $obj->nama      = "Redian Galih Irianti";
        $obj->tipe      = "1";
        $obj->email     = "rediangalih96@gmail.com";
        $obj->telepon   = "81675438";
        $obj->alamat    = "Tegal Mulyorejo Baru 114 Mulyorejo";
        return $obj;
    }
}
?>