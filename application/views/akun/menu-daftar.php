
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<section id="daftar">
<header class="intro-header" style="background-image: url('<?= base_url('assets/img/daftar.jpg');?>')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <span class="subheading">
                        <em>Daftar Akun </em>
                    </span>
                    <h1>Pemilik | Pencari</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="row">
                <div class="col-md-4">
                    <div class="form-group col-xs-12">
                        <a href="<?= base_url();?>daftar/pemilik">
                            <button class="btn btn-default">daftar sebagai Pemilik Kos</button>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class="form-group col-xs-12">
                        <a href="<?= base_url();?>daftar/pencari">
                        <button class="btn btn-default">daftar sebagai Pencari Kos</button>
                        </a>
                    </div>
                </div>
        </div>
    </div>
</div>
</section>
<hr>
	
	