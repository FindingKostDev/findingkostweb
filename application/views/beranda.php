
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('<?php base_url();?>assets/img/home1.jpg')">>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <br>
                    <hr class="small">
                    <h1>FindingKost</h1>
                    <hr class="small">
                    <span class="subheading" style="">
                        <em>Dapatkan hunian yang anda inginkan</em>
                    </span>
                    <br>
                    <div class="form-group col-xs-12">
                        <a href="<?= base_url('kos/cari'); ?>">
                            <button type="submit" class="btn btn-default">
                                <em>Cari</em>
                            </button>
                        </a>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</header>
