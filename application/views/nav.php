

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid" 
         style="background:#FFFFFF;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" 
                    class="navbar-toggle" 
                    data-toggle="collapse" 
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" 
               href="<?= base_url();?>" 
               style="color:#524F50;">
                FindingKos
            </a>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" 
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?= base_url('kos/cari');?>" 
                       style="color:#524F50;" >
                        Pencarian Cepat
                    </a>
                </li>
                <?php if($this->session->has_userdata('username')) {?>
                
                <li>
                    <a href="<?= base_url('akun/profil/'.$this->session->username) ?>" 
                       style="color: #0092DD;" >
                        <?=  $this->session->nama; ?>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url('akun/keluar') ?>" 
                       style="color: #524F50;" >
                        Keluar
                    </a>
                </li>
                <?php } else { ?>
                <li>
                    <a href="<?= base_url('akun/masuk') ?>" 
                       style="color: #524F50;" >
                        Masuk
                    </a>
                </li>
                <li>
                    <a href="<?= base_url('daftar') ?>" 
                       style="color: #524F50;" >
                        Daftar
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>