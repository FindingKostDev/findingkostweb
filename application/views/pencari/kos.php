
     <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    
                    <h2 class="section-heading">KOS YANG DIHUNI</h2>
                        <br>
					    <div class="row">
								<table>
									<tr>
										<th width="600px">Alamat</th>
										<th width="200px">Daerah</th>
										<th width="200px">Harga</th>
									</tr>
									<tr>
										<td padding="30px">
											<?= $kost->alamat; ?>
										</td>
										<td>
											<?= $kost->daerah; ?>
										</td>
										<td>
											Rp <?php echo number_format($kost->harga,0,',','.'); ?>
										</td>	
									</tr>
									
								</table>
					    </div>
                
                </div>
            </div>
        </div>
           
     </div>