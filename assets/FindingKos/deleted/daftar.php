
<!-- Page Header -->
<!-- Set your background image for this header on the line below. --
<section id="daftar">
<!--header class="intro-header" style="background-image: url('img/DaftarPemilikKos.jpg')"--
<br><br><hr><div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Daftar Akun</h1>
                    <span class="subheading">
                        <em>
                            sebagai <?= $akun ?> Kos
                        </em>
                    </span>
                </div>
            </div>
        </div>
    </div>
<!--/header>

<!-- Main Content --
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

            <!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
            <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
            <!-- NOTE: To use the contact form, your site must be on a live web host with PHP! The form will not work locally! --
            <?php //form_open('daftar/submit_daftar'); ?>
            <?php //validation_errors(); ?>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Nama Lengkap</label>
                    <input 
                        type="text" 
                        name="nama" 
                        class="form-control" 
                        placeholder="Nama Lengkap" 
                        id="nama" 
                        required 
                        data-validation-required-message="Masukkan Nama Lengkap Anda.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Nama Pengguna</label>
                    <input 
                        type="text" 
                        name="username" 
                        class="form-control" 
                        placeholder="Nama Pengguna" 
                        id="username" 
                        required 
                        data-validation-required-message="Masukkan Nama Pengguna Anda.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Alamat Email</label>
                    <input 
                        type="email" 
                        name="email" 
                        class="form-control" 
                        placeholder="Alamat Email" 
                        id="emaiil" 
                        required data-validation-required-message="Masukkan Alamat Email Anda">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Kata Sandi</label>
                    <input 
                        type="password" 
                        name="password" 
                        class="form-control" 
                        placeholder="Kata Sandi" 
                        id="password" 
                        required 
                        data-validation-required-message="Masukkan Kata Sandi Anda.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Ulangi Kata Sandi</label>
                    <input 
                        type="password" 
                        name="passconf" 
                        class="form-control" 
                        placeholder="Ulangi Kata Sandi" 
                        id="passconf" 
                        required 
                        data-validation-required-message="Ulangi Kata Sandi Anda">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Nomor Telepon</label>
                    <input 
                        type="number" 
                        name="telepon" 
                        class="form-control" 
                        placeholder="Nomor Telepon" 
                        id="telepon" 
                        required 
                        data-validation-required-message="Masukkan Nomor Telepon Anda">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Alamat</label>
                    <input 
                        type="textarea" 
                        name="alamat" 
                        class="form-control" 
                        placeholder="Alamat" 
                        id="alamat" 
                        required 
                        data-validation-required-message="Masukkan Alamat Anda.">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <input 
                type="hidden" 
                name="tipe" 
                value="<?php //$tipe; ?>">
            <div id="success"></div>
            <div class="row">
                <div class="form-group col-xs-12">
                    <?php //form_submit('submit_daftar', 'Daftar', array('class'=>"btn btn-default")); ?>
                </div>
            </div>
            <?php //form_close(); ?>
        </div>
    </div>
</div>
</section>
<hr>